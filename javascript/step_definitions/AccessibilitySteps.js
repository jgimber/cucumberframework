// Basic steps to support accessibility checking of a website.

const { When, Then } = require('@cucumber/cucumber');

const { logger } = require('../scope/Logger');
const { assessPageAccessibility, generateAccessibilityReport } = require('../action/Accessibility');
const { navigateToFullUrl } = require('../Support');

When('the user navigates to the {string} page at the URL {string}', async function (pageName, pageURL) {
    logger.info(`Navigating to the ${pageName} page at this URL: ${pageURL}`);
    await navigateToFullUrl.call(this, pageURL);
});

When('the page is checked for Accessibility', async function () {
    await assessPageAccessibility.call(this);
});

Then('the {string} screen element is compliant with the Accessibility standards', async function (componentName) {
    await generateAccessibilityReport.call(this, componentName);
});

Then('the {string} page is compliant with the Accessibility standards', async function (pageName) {
    await generateAccessibilityReport.call(this, pageName);
});
