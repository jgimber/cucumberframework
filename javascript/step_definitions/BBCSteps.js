// Steps to support a demonstration of testing the BBC website for accessibility.

const { Given } = require('@cucumber/cucumber');
const { assessPageElementAccessibility } = require('../action/Accessibility');
const { logger } = require('../scope/Logger');
const { navigateToFullUrl } = require('../Support');

Given('the user navigates to the BBC homepage', async function () {
    await navigateToFullUrl.call(this, 'https://www.bbc.co.uk/');
});

Given('the user navigates to the BBC weather page', async function () {
    await navigateToFullUrl.call(this, 'https://www.bbc.co.uk/weather');
});

Given('the user searches for weather in {string}', async function (location) {
    logger.info(`Searching for weather in: ${location}`);
    await this.page.type('input[id="ls-c-search__input-label"]', location);
    await this.page.waitForXPath('//*[@id="location-list"]');

    // Short wait, to display the location list to the tester.  (DEMO PURPOSES)
    // await this.page.waitForTimeout(20000);
});

Given('the location search results panel is checked for Accessibility', async function () {
    // Check the Location search panel, with locations displayed.
    const elementsToCheck = ['#wr-location-search-container'];
    await assessPageElementAccessibility.call(this, elementsToCheck);
});
