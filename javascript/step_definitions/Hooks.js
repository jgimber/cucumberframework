// This file supports all test-based administration, e.g.
// - Setup of the browser (pre-test),
// - Initialising the State object ready for transferring data between isolated test steps (pre-test),
// - Test teardown (after-test),
// - Attaching screenshots to the Cucumber HTML report (in the event of test failure).

const { Before, setDefaultTimeout, setWorldConstructor, After } = require('@cucumber/cucumber');
const { logger } = require('../scope/Logger');
const FeatureScope = require('../scope/FeatureScope');
const BrowserScope = require('../scope/BrowserScope');

setDefaultTimeout(70000);
setWorldConstructor(BrowserScope);
const featureScope = new FeatureScope();

async function setUpBrowser() {
    await featureScope.init(this.currentFeature, this.worldParameters);
    this.page = featureScope.browserScope.page;
    this.page.setDefaultTimeout(10000);
    this.page.setDefaultNavigationTimeout(30000);
    this.browser = featureScope.browserScope.browser;

    // Set up a store area to use between test steps.
    this.state = {
        accessibility: {}
    };
}

Before(async function beforeEach(scenario) {
    // Check if the current scenario is the same feature file.
    const currentFeature = scenario.gherkinDocument.uri;
    logger.info(`[FEATURE]\t${currentFeature}`);
    logger.info(`[SCENARIO]\t${scenario.pickle.name}`);

    await setUpBrowser.call(this);

    await this.page.setViewport({
        width: 1200,
        height: 1900
    });
});

After(async function afterEach(scenario) {
    // If the scenario failed, take a snapshot and attach it to the results.
    if (scenario.result.status.toUpperCase() === 'FAILED') {
        const screeshot = await this.page.screenshot();
        await this.attach(screeshot, 'image/png');

        // In Jenkins, this report will automatically be created as the output report.
        // To generate the report locally, refer to the instructions in README.MD
    }
    await featureScope.browserScope.close();
});
