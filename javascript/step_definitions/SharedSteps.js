// This file contains basic steps that can be used for any test, not just a specific page-related test.

const { Given } = require('@cucumber/cucumber');
const { navigateToFullUrl } = require('../Support');

Given('the user navigates to {string}', async function (targetURL) {
    await navigateToFullUrl.call(this, targetURL);
});
