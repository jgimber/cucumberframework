// Support methods which assist with basic test steps.  These are not specific to any page / site.

const assert = require('assert');
const { logger } = require('./scope/Logger');

/**
 * This method waits until the destination page URL has been reached.
 * This avoids any issues with checks or operations being performed on a page while it is still redirecting.
 */
async function verifyCurrentUrl(requiredURL) {
    logger.trace(`Waiting for the URL to become: ${requiredURL}`);
    let foundURL;
    async function waitForRedirectionToComplete() {
        try {
            foundURL = '';
            foundURL = await this.page.url();
        } catch (error) {
            // Usually here if the previous operation failed due to a page redirect happening.
            logger.warn('- Recovering during page redirection...');
        }
        if (foundURL.includes(requiredURL) === false) {
            // We have a URL, but it's not the required one.
            logger.trace('Redirecting...');
            await this.page.waitForTimeout(500);
            await waitForRedirectionToComplete.call(this);
        }
    }
    await waitForRedirectionToComplete.call(this);

    // We now have completed the redirection, and have a legit new URL.
    // Double-check it is correct.
    assert.equal(foundURL, requiredURL, `The new URL (${foundURL})does not match the expected url (${requiredURL})`);
}

/**
 * Navigate to a specific URL, and verify that the target location has been reached.
 *
 * @param {String} url      The URL to navigate to.
 */
async function navigateToFullUrl(url) {
    logger.info(`Navigating to: ${url}`);
    await Promise.all([
        this.page.goto(url),
        this.page.waitForNavigation()
    ]);
    await verifyCurrentUrl.call(this, url);
}

module.exports = {
    navigateToFullUrl
};
