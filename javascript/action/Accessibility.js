// Methods to support accessibility checking.

const assert = require('assert');
const { createHtmlReport } = require('axe-html-reporter');
const { AxePuppeteer } = require('@axe-core/puppeteer');
const { logger } = require('../scope/Logger');

// The accessibility, and accessibility report options are contained in package.json.
const { accessibilityOptions, accessibilityReportOptions } = require('../../package.json');

/**
 * This function will check the current webpage for accessibility.
 * Results are stored for later analysis.
 *
 * @param {String[]} excludeScreenElements  An array of all items on the screen to EXCLUDE from the page check.
 */
async function assessPageAccessibility(excludeScreenElements = []) {
    logger.info('Checking page for Accessibility');
    // Add the list of excluded screen elements to the "default set" of screen elements to exclude
    // which are defined in package.json.
    const excludeElements = excludeScreenElements.concat(accessibilityOptions.excludedScreenElements);

    // Analyse the page, given the exclusions and other settings.
    const results = await new AxePuppeteer(this.page)
    .withTags(accessibilityOptions.standards)
    .disableRules(accessibilityOptions.excludedRules)
    .exclude(excludeElements)
    .analyze();

    // Store the results in the State object, so that it may be picked up and analysed by other steps.
    this.state.accessibility.accessibilityResults = results;
    this.state.accessibility.includedScreenElements = null;
    this.state.accessibility.excludedScreenElements = excludeElements;
}

/**
 * Instead of checking a whole page for accessibility, just check the list of items to focus on.
 * Results are stored for later analysis.
 *
 * @param {String[]} includeScreenElements  The list of elements on-page to check for accessibility.
 */
async function assessPageElementAccessibility(includeScreenElements) {
    logger.info(`Checking page element(s) for Accessibility: ${includeScreenElements}`);

    // Use the checking and reporting options as specified in package.json
    const results = await new AxePuppeteer(this.page)
    .withTags(accessibilityOptions.standards)
    .disableRules(accessibilityOptions.excludedRules)
    .include(includeScreenElements)
    .analyze();

    // Store the results in the State object, so that it may be picked up and analysed by other steps.
    this.state.accessibility.accessibilityResults = results;
    this.state.accessibility.includedScreenElements = includeScreenElements;
    this.state.accessibility.excludedScreenElements = accessibilityOptions.excludeScreenElements;
}

/**
 * Create an HTML banner to include on the accessibility report in the header.
 *
 * @param {String} reportName   The name of the item being reported on, e.g. a screen or screen element.
 * @returns String              The HTML fragment to include in the report header.
 */
function getHTMLSummaryForReport(reportName) {
    logger.trace('Creating HTML insert for accessibility report');
    let bannerHTML = `<h5>Summary of options used for test of: ${reportName}</h5>`;
    bannerHTML += `<ul><li>Accessibility standards checked: ${accessibilityOptions.standards}</li>`;
    bannerHTML += `<li>Accessibility rules excluded: ${accessibilityOptions.excludedRules}</li>`;
    if (this.state.accessibility.excludedScreenElements) {
        bannerHTML += `<li>Excluded screen elements: ${this.state.accessibility.excludedScreenElements}</li>`;
    }
    if (this.state.accessibility.includedScreenElements) {
        bannerHTML += `<li>Included screen elements: ${this.state.accessibility.includedScreenElements}</li>`;
    }
    bannerHTML += '</ul>';
    return bannerHTML;
}

/**
 * Assuming that the accessibility check has been run (either on a screen, or screen element),
 * this will analyse the results and generate a report IF there are accessibility failures detected.
 * Reports are HTML-based, interactive reports.
 * Report configuration data is taken from package.json.
 *
 * @param {String} itemName     The name of the thing being reported on.
 */
function generateAccessibilityReport(itemName) {
    logger.info(`Assessing Accessibility information for: ${itemName}`);
    const { accessibilityResults } = this.state.accessibility;
    const issuesFound = accessibilityResults.violations.length;

    if (issuesFound > 0) {
        // Failures were found on the page.
        logger.error(`${issuesFound} Accessibility violations were detected in the Page / Component: ${itemName}`);

        // List the rule violations.
        const ruleViolations = accessibilityResults.violations;
        ruleViolations.forEach((rule, index) => {
            logger.error(`${index + 1}: ${rule.impact}\t${rule.help}`);
        });

        // Generage a page-specific html report for it.
        accessibilityReportOptions.reportFileName = `${itemName}.html`;
        accessibilityReportOptions.customSummary = getHTMLSummaryForReport.call(this, itemName);
        createHtmlReport({
            results: accessibilityResults,
            options: accessibilityReportOptions
        });

        // Test has failed, as this is not accessible.
        assert.fail('Page / Component not accessibility-friendly.');
    }
}

module.exports = {
    assessPageAccessibility,
    assessPageElementAccessibility,
    generateAccessibilityReport
};
