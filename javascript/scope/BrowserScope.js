// This file details how the browser is launched, and handles state information for the test.

const puppeteer = require('puppeteer');

const { puppeteerOptions } = require('../../package.json');
const { logger } = require('./Logger');

class BrowserScope {
    constructor(args) {
        this.browser = null;
        this.config = null;
        this.worldParameters = args && args.parameters ? args.parameters : {};
        this.attach = args && args.attach ? args.attach : null;
    }

    async init() {
        const defaultOptions = {
            args: ['--no-sandbox', '--disable-dev-shg-usage'],
            ignoreHTTPSErrors: true
        };
        await this.close();

        // Launch the browser, and target the default tab.
        logger.trace('Launching new browser instance...');
        this.browser = await puppeteer.launch(puppeteerOptions || defaultOptions);
        [this.page] = await this.browser.pages();
        this.state = {};
    }

    async close() {
        if (this.browser) {
            await this.browser.close();
        }
        this.browser = null;
        this.page = null;
        this.state = null;
    }
}

module.exports = BrowserScope;
