// This defines the Logger library that is used by all other code modules for logging messages.

const log4js = require('log4js');

const {
    LOGGING_LEVEL
} = require('../../BaseConfig');

const logger = log4js.getLogger();

log4js.configure({
    appenders: {
        out: {
            type: 'stdout',
            layout: {
                type: 'pattern',
                // Specific logger message pattern:
                // %[ %] - use colour for each logging level - applied to anything within the block.
                // %d    - use the current date and timestamp.
                // %z    - process ID, used to track issues during parallel execution.
                // %p    - logger level.
                // %f{1} - the filename.  NOTE: %f would shown the full path, this shows just the filename.
                // %l    - the line number being executed.
                // %m    - the actual log message.
                pattern: '%[%d [%p] [%z] [%f{1}:%l] - %m%]'
                // Sample log message with the current format:
                // 2022-04-01T11:12:13.123 [123456] [INFO] [MyFile.js:123] - My message text
            }
        }
    },
    categories: {
        default: {
            appenders: ['out'],
            // Set the level to the required filter level.
            // e.g. "info" would display "info" and higher level messages, filtering out lower-level trace messages.
            // Allowable values:
            // OFF: nothing is logged.
            // ALL: everything is logged.
            // TRACE: anything logged with logger.trace(msg) (and higher) is logged.
            // DEBUG: anything logged with logger.debug(msg) (and higher) is logged.
            // INFO: anything logged with logger.debug(msg) (and higher) is logged.
            // WARN: anything logged with logger.debug(msg) (and higher) is logged.
            // ERROR: anything logged with logger.debug(msg) (and higher) is logged.
            // FATAL: anything logged with logger.debug(msg) (and higher) is logged.
            //
            // Recommended usage:
            // .trace(msg) for checks / verifications.
            // .info(msg) for actions.
            // .debug(msg) for troubleshooting / development.
            level: LOGGING_LEVEL,
            enableCallStack: true
        }
    }
});

// Export the defined logger so that other modules can use it.
//
// To use this logger in a different file, these lines are required in it:
// const { logger } = require('<pathTo>/scope/Logger)');
// logger.info('This is my message');
exports.logger = logger;
