const BrowserScope = require('./BrowserScope');
const { logger } = require('./Logger');

class FeatureScope {
    constructor() {
        this.browserScope = null;
        this.feature = null;
    }

    isNewFeature(currentFeature) {
        console.log(currentFeature);
        console.log(this.feature);
        return this.feature !== currentFeature;
    }

    async init(currentFeature, worldParameters) {
        this.feature = currentFeature;
        this.worldParameters = worldParameters;

        if (this.browserScope) {
            logger.trace('Closing existing browser...');
            await this.browserScope.close();
            this.browserScope = null;
        }

        this.browserScope = new BrowserScope({ parameters: worldParameters });
        await this.browserScope.init();
    }
}

module.exports = FeatureScope;
