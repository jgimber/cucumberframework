**Accessibility test framework information**

## Running accessibility tests

```sh
node_modules/@cucumber/cucumber/bin/cucumber-js resources/features/accessibility --require javascript/step_definitions --format json:./reports/cucumber-report.json --tags 'not @ignore'
```

## Running accessibility tests in parallel

A speedup can be gained for test execution by running tests in parallel.  To do this, add "--parallel x" (where x is the number of parallel streams to execute).  For example:

```sh
node_modules/@cucumber/cucumber/bin/cucumber-js resources/features/accessibility --require javascript/step_definitions --format json:./reports/cucumber-report.json --tags 'not @ignore' --parallel 8
```

## Suggested extensions for VSCode

ESLint
Cucumber (Gherkin) Full Language Support + Formatting + Autocomplete
