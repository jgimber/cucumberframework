const dotenv = require('dotenv');

dotenv.config();
const {
    // The values in here are extracted from the file ".env" in the root of the project.
    // They can be used to configure the framework to run according to 
    // URL to use for accessibility testing.
    BASE_URL,
    LOGGING_LEVEL
} = process.env;

module.exports = {
    BASE_URL,
    LOGGING_LEVEL
};
