Feature: Example accessibility check of BBC website
    As an accessibility tester
    When I test the BBC site
    Then the accessibility level can be determined

    # Example 1: Go to a single page, and check that page only.
    Scenario: 1. The BBC homepage is accessible
        Given the user navigates to the BBC homepage
        When the page is checked for Accessibility
        Then the "BBC home" page is compliant with the Accessibility standards

    # Example 2: Go to multiple pages, and check each one in turn.
    Scenario Outline: Basic page is accessible: <pageName>
        Given the user navigates to '<pageUrl>'
        When the page is checked for Accessibility
        Then the '<pageName>' page is compliant with the Accessibility standards

        Examples:
            | pageUrl                       | pageName     |
            | https://www.bbc.co.uk/        | BBC Homepage |
            | https://www.bbc.co.uk/news    | BBC News     |
            | https://www.bbc.co.uk/weather | BBC Weather  |

@sjg
    # Example 3: Check a single screen element for accessibility.
    Scenario: 2. The BBC Weather location search is accessible
        Given the user navigates to the BBC weather page
        And the user searches for weather in "Swansea"
        And the location search results panel is checked for Accessibility
        Then the "BBC Weather page - location search results" screen element is compliant with the Accessibility standards
