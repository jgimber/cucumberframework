Feature: Example accessibility check of generic website
    As an accessibility tester
    When I test a generic site
    Then the accessibility level can be determined

    Scenario: 1. The website is accessible
        Given the user navigates to 'https://www.bbc.co.uk/'
        When the page is checked for Accessibility
        Then the 'BBC home' page is compliant with the Accessibility standards           
